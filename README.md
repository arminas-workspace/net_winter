# Visma
This is a Visma intern application task.

## How to use?
After launching console application you will have 4 choices:
```
'stock' - puts you into stock table
'menu' - puts you into menu table
'orders' - puts you into orders table
'exit' - exits the program
```
After moving into one of the tables you have 5 new options:
```
'show all' - shows all the elements of the table
'show one' - will ask you which element to find by id, then you give it an id number
'add' - adding of a new table element, you will have to put in the parts of element one by one
'update' - ask which table element to update, then you have to update all parts of the element
'delete' - ask which element to delete, you have to input an id number
'exit' - goes back to table selection
```

## Example
```
>You are now in base menu
stock
>Set to use stock table
show all
>Showing all stock items
|   Id|                     Name| Portion Count|      Unit|  Portion Size|
|    1|                  Chicken|             7|        kg|           0.3|
|    2|                 Potatoes|          78.5|        kg|           0.5|
|    3|                  Cabbage|           7.6|        kg|           0.3|
|    4|                     Cola|            25|         l|             1|
|    5|                 Tomatoes|          19.8|        kg|           0.2|
|    6|                      Ham|            20|        kg|           0.1|
exit
>Exiting stock table
orders
>Set to use orders table
show all
>Showing all orders
|   Id|                Date time|    Menu items|
|    1|      2020-12-28 23:40:00|             2
|    2|      2020-12-28 22:23:00|           1 3
|    4|      2020-12-29 15:30:00|           1 2
|    5|      2020-12-30 20:15:00|           1 4
|    6|      2020-12-30 15:00:00|             1
add
>Add new order:
>Give new order Id:
7
>Give order a date and time:
2020-12-30 16:30
>Give order menu items:
1 3
>Failed to add new Order
add
>Add new order:
>Give new order Id:
7
>Give order a date and time:
2020-12-30 16:30
>Give order menu items:
1 2
>New Order added successfully
show all
>Showing all orders
|   Id|                Date time|    Menu items|
|    1|      2020-12-28 23:40:00|             2
|    2|      2020-12-28 22:23:00|           1 3
|    4|      2020-12-29 15:30:00|           1 2
|    5|      2020-12-30 20:15:00|           1 4
|    6|      2020-12-30 15:00:00|             1
|    7|      2020-12-30 16:30:00|           1 2
```