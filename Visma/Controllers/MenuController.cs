﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Visma.Entities;
using Visma.Services;

namespace Visma.Controllers
{
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IService<Menu> _menuService;
        public MenuController()
        {
            _menuService = new MenuService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAll()
        {
            var menu = _menuService.GetAll();
            return Ok(menu);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetOne(int id)
        {
            var menu = _menuService.GetOne(id);
            if (menu == null)
            {
                return NotFound($"Menu with id: {id} was not found");
            }
            return Ok(menu);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put(Menu menu)
        {
            var status = _menuService.Put(menu);
            if (!status)
            {
                return BadRequest($"Bad request, new menu order was not added");
            }
            return Ok(menu);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update(int id, Menu menu)
        {
            var status = _menuService.Update(id, menu);
            if (!status)
            {
                return BadRequest($"Bad request, new menu order was not updated");
            }
            return Ok(menu);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            var status = _menuService.Delete(id);
            if (!status)
            {
                return NotFound($"404, menu not found");
            }
            return Ok(status);
        }
    }
}
