﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Visma.Entities;
using Visma.Services;

namespace Visma.Controllers
{
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IService<Stock> _stockService;
        public StockController()
        {
            _stockService = new StockService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllStock()
        {
            var stock = _stockService.GetAll();
            return Ok(stock);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetOneStock(int id)
        {
            var stock = _stockService.GetOne(id);
            if(stock == null)
            {
                return NotFound($"Stock with id: {id} was not found");
            }
            return Ok(stock);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult PutNewStock(Stock stock)
        {
            var status = _stockService.Put(stock);
            if (!status)
            {
                return BadRequest($"Bad request, new stock was not added");
            }
            return Ok(stock);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateStock(int id, Stock stock)
        {
            var status = _stockService.Update(id, stock);
            if (!status)
            {
                return BadRequest($"Bad request, new stock was not updated");
            }
            return Ok(stock);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult RemoveStock(int id)
        {
            var status = _stockService.Delete(id);
            if (!status)
            {
                return NotFound($"404, stock not found");
            }
            return Ok(status);
        }
    }
}
