﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Visma.Entities;
using Visma.Services;

namespace Visma.Controllers
{
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IService<Order> _ordersService;
        public OrdersController()
        {
            _ordersService = new OrdersService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAll()
        {
            var Orders = _ordersService.GetAll();
            return Ok(Orders);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetOne(int id)
        {
            var orders = _ordersService.GetOne(id);
            if (orders == null)
            {
                return NotFound($"Orders with id: {id} was not found");
            }
            return Ok(orders);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put(Order orders)
        {
            var status = _ordersService.Put(orders);
            if (!status)
            {
                return BadRequest($"Bad request, new order was not added");
            }
            return Ok(orders);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update(int id, Order orders)
        {
            var status = _ordersService.Update(id, orders);
            if (!status)
            {
                return BadRequest($"Bad request, order was not updated");
            }
            return Ok(orders);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            var status = _ordersService.Delete(id);
            if (!status)
            {
                return NotFound($"404, Order not found");
            }
            return Ok(status);
        }
    }
}
