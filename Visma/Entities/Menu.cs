﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Visma.Entities
{
    public class Menu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> Products { get; set; }

        public override string ToString()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}", Id, Name, String.Join(' ', Products.ToArray()));
        }
    }
}
