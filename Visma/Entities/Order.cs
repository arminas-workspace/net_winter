﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Visma.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public List<int> MenuItems { get; set; }
        public override string ToString()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}", Id, DateTime, String.Join(' ', MenuItems.ToArray()));
        }
    }
}
