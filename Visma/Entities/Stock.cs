﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Visma.Entities
{
    public class Stock
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PortionCount { get; set; }
        public string Unit { get; set; }
        public double PortionSize { get; set; }

        public override string ToString()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}|{3,10}|{4,14}|", Id, Name, Math.Round(PortionCount, 2), Unit, PortionSize);
        }
    }
}
