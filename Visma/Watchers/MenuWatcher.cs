﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visma.Controllers;
using Visma.Entities;

namespace Visma.Watchers
{
    public class MenuWatcher : IWatcher
    {
        private readonly MenuController _menuController;
        public MenuWatcher()
        {
            _menuController = new MenuController();
        }

        public void BeginWatching()
        {
            string input = "";

            while (input.ToLower().CompareTo("exit") != 0)
            {
                input = Console.ReadLine();
                int id = 0;
                switch (input.ToLower())
                {
                    case "show all":
                        Console.WriteLine(">Showing all Menu items");
                        var Menu = GetAll();
                        Console.WriteLine(DescribeTable());
                        foreach (var item in Menu)
                        {
                            Console.WriteLine(item.ToString());
                        }
                        break;
                    case "show one":
                        Console.WriteLine(">Write in Menu item id to find:");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        var menuOne = GetOne(id);
                        if (menuOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(menuOne.ToString());
                        }
                        else
                            Console.WriteLine(">404 nothing found");
                        break;
                    case "add":
                        Menu newMenu = new Menu();
                        Console.WriteLine(">Add new menu selection:");
                        Console.WriteLine(">Give new menu item Id:");
                        newMenu.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give menu item a name:");
                        newMenu.Name = Console.ReadLine();
                        Console.WriteLine(">Give menu item products:");
                        string val = Console.ReadLine();
                        newMenu.Products = val.Split(' ').Select(Int32.Parse).ToList();
                        var status = Put(newMenu);
                        if (status)
                            Console.WriteLine(">New menu choice added successfully");
                        else
                            Console.WriteLine(">Failed to add new menu choice");
                        break;
                    case "update":
                        Console.WriteLine(">Which item would you like to update?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        menuOne = GetOne(id);
                        if (menuOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(menuOne.ToString());
                        }
                        else
                        {
                            Console.WriteLine(">404 nothing found");
                            break;
                        }
                        newMenu = new Menu();
                        Console.WriteLine(">Update given product:");
                        Console.WriteLine(">Give menu item new Id:");
                        newMenu.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give menu item a new name:");
                        newMenu.Name = Console.ReadLine();
                        Console.WriteLine(">Give menu item new products:");
                        newMenu.Products = Console.ReadLine().Split(' ').Select(Int32.Parse).ToList();
                        status = Update(id, newMenu);
                        if (status)
                            Console.WriteLine(">Menu updated successfully");
                        else
                            Console.WriteLine(">Failed to update Menu");
                        break;
                    case "delete":
                        Console.WriteLine(">Which menu item would you like to delete?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Deleting element...");
                        if (Delete(id))
                            Console.WriteLine(">Menu item removed successfully");
                        else
                            Console.WriteLine(">Menu item not found");
                        break;
                    case "exit":
                        Console.WriteLine(">Exiting Menu table");
                        break;
                    default:
                        Console.WriteLine(">Invalid command, please input using the correct format!");
                        break;
                }
            }
        }

        private bool Update(int id, Menu newMenu)
        {
            var status = _menuController.Update(id, newMenu);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Delete(int id)
        {
            var status = _menuController.Delete(id);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Put(Menu Menu)
        {
            var status = _menuController.Put(Menu);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private string DescribeTable()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}", "Id", "Name", "Products");
        }

        private Menu GetOne(int id)
        {
            var Menu = _menuController.GetOne(id);
            if (Menu.GetType() == typeof(OkObjectResult))
            {
                var okResult = Menu as OkObjectResult;
                return okResult.Value as Menu;
            }
            return null;
        }

        private IList<Menu> GetAll()
        {
            var Menu = _menuController.GetAll();
            var okResult = Menu as OkObjectResult;
            var result = okResult.Value as IList<Menu>;
            return result.OrderBy(x => x.Id).ToList();
        }
    }
}
