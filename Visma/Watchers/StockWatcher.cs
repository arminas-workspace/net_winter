﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visma.Controllers;
using Visma.Entities;

namespace Visma.Watchers
{
    public class StockWatcher : IWatcher
    {
        private readonly StockController _stockController;
        public StockWatcher()
        {
            _stockController = new StockController();
        }

        public void BeginWatching()
        {
            string input = "";

            while (input.ToLower().CompareTo("exit") != 0)
            {
                input = Console.ReadLine();
                int id = 0;
                switch (input.ToLower())
                {
                    case "show all":
                        Console.WriteLine(">Showing all stock items");
                        var stock = GetAll();
                        Console.WriteLine(DescribeTable());
                        foreach (var item in stock)
                        {
                            Console.WriteLine(item.ToString());
                        }
                        break;
                    case "show one":
                        Console.WriteLine(">Write in stock item id to find:");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        var stockOne = GetOne(id);
                        if (stockOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(stockOne.ToString());
                        }
                        else
                            Console.WriteLine(">404 nothing found");
                        break;
                    case "add":
                        Stock newStock = new Stock();
                        Console.WriteLine(">Add new product:");
                        Console.WriteLine(">Give new product Id:");
                        newStock.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give product a name:");
                        newStock.Name = Console.ReadLine();
                        Console.WriteLine(">Give product portion count:");
                        newStock.PortionCount = double.Parse(Console.ReadLine());
                        Console.WriteLine(">Give unit description:");
                        newStock.Unit = Console.ReadLine();
                        Console.WriteLine(">Give product portion size:");
                        newStock.PortionSize = double.Parse(Console.ReadLine());
                        var status = Put(newStock);
                        if (status)
                            Console.WriteLine(">New stock added successfully");
                        else
                            Console.WriteLine(">Failed to add new stock");
                        break;
                    case "update":
                        Console.WriteLine(">Which item would you like to update?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        stockOne = GetOne(id);
                        if (stockOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(stockOne.ToString());
                        }
                        else
                        {
                            Console.WriteLine(">404 nothing found");
                            break;
                        }
                        newStock = new Stock();
                        Console.WriteLine(">Update given product:");
                        Console.WriteLine(">Give product new Id:");
                        newStock.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give product a new name:");
                        newStock.Name = Console.ReadLine();
                        Console.WriteLine(">Give product a new portion count:");
                        newStock.PortionCount = double.Parse(Console.ReadLine());
                        Console.WriteLine(">Give unit description:");
                        newStock.Unit = Console.ReadLine();
                        Console.WriteLine(">Give product a new portion size:");
                        newStock.PortionSize = double.Parse(Console.ReadLine());
                        status = Update(id, newStock);
                        if (status)
                            Console.WriteLine(">Stock updated successfully");
                        else
                            Console.WriteLine(">Failed to update stock");
                        break;
                    case "delete":
                        Console.WriteLine(">Which item would you like to delete?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Deleting element...");
                        if (Delete(id))
                            Console.WriteLine(">Stock remove successfully");
                        else
                            Console.WriteLine(">Stock not found");
                        break;
                    case "exit":
                        Console.WriteLine(">Exiting stock table");
                        break;
                    default:
                        Console.WriteLine(">Invalid command, please input using the correct format!");
                        break;
                }
            }
        }

        private bool Update(int id, Stock newStock)
        {
            var status = _stockController.UpdateStock(id, newStock);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Delete(int id)
        {
            var stock = _stockController.RemoveStock(id);
            if (stock.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Put(Stock stock)
        {
            var status = _stockController.PutNewStock(stock);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private string DescribeTable()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}|{3,10}|{4,14}|", "Id", "Name", "Portion Count", "Unit", "Portion Size");
        }

        private Stock GetOne(int id)
        {
            var stock = _stockController.GetOneStock(id);
            if(stock.GetType() == typeof(OkObjectResult))
            {
                var okResult = stock as OkObjectResult;
                return okResult.Value as Stock;
            }
            return null;
        }

        private IList<Stock> GetAll()
        {
            var stock = _stockController.GetAllStock();
            var okResult = stock as OkObjectResult;
            var result = okResult.Value as IList<Stock>;
            return result.OrderBy(x => x.Id).ToList();
        }
    }
}
