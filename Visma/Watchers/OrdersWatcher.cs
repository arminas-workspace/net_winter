﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visma.Controllers;
using Visma.Entities;

namespace Visma.Watchers
{
    public class OrdersWatcher : IWatcher
    {
        private readonly OrdersController _ordersController;
        public OrdersWatcher()
        {
            _ordersController = new OrdersController();
        }

        public void BeginWatching()
        {
            string input = "";

            while (input.ToLower().CompareTo("exit") != 0)
            {
                input = Console.ReadLine();
                int id = 0;
                switch (input.ToLower())
                {
                    case "show all":
                        Console.WriteLine(">Showing all orders");
                        var orders = GetAll();
                        Console.WriteLine(DescribeTable());
                        foreach (var item in orders)
                        {
                            Console.WriteLine(item.ToString());
                        }
                        break;
                    case "show one":
                        Console.WriteLine(">Write in order id to find:");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        var ordersOne = GetOne(id);
                        if (ordersOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(ordersOne.ToString());
                        }
                        else
                            Console.WriteLine(">404 nothing found");
                        break;
                    case "add":
                        Order newOrder = new Order();
                        Console.WriteLine(">Add new order:");
                        Console.WriteLine(">Give new order Id:");
                        newOrder.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give order a date and time:");
                        newOrder.DateTime = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine(">Give order menu items:");
                        newOrder.MenuItems = Console.ReadLine().Split(' ').Select(Int32.Parse).ToList();
                        var status = Put(newOrder);
                        if (status)
                            Console.WriteLine(">New Order added successfully");
                        else
                            Console.WriteLine(">Failed to add new Order");
                        break;
                    case "update":
                        Console.WriteLine(">Which order would you like to update?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Searching for item...");
                        ordersOne = GetOne(id);
                        if (ordersOne != null)
                        {
                            Console.WriteLine(DescribeTable());
                            Console.WriteLine(ordersOne.ToString());
                        }
                        else
                        {
                            Console.WriteLine(">404 nothing found");
                            break;
                        }
                        newOrder = new Order();
                        Console.WriteLine(">Update given order:");
                        Console.WriteLine(">Give order a new Id:");
                        newOrder.Id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Give order a new date and time:");
                        newOrder.DateTime = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine(">Give order menu items:");
                        newOrder.MenuItems = Console.ReadLine().Split(' ').Select(Int32.Parse).ToList();
                        status = Update(id, newOrder);
                        if (status)
                            Console.WriteLine(">Orders updated successfully");
                        else
                            Console.WriteLine(">Failed to update Orders");
                        break;
                    case "delete":
                        Console.WriteLine(">Which item would you like to delete?");
                        id = int.Parse(Console.ReadLine());
                        Console.WriteLine(">Deleting element...");
                        if (Delete(id))
                            Console.WriteLine(">Order remove successfully");
                        else
                            Console.WriteLine(">Order not found");
                        break;
                    case "exit":
                        Console.WriteLine(">Exiting Orders table");
                        break;
                    default:
                        Console.WriteLine(">Invalid command, please input using the correct format!");
                        break;
                }
            }
        }

        private bool Update(int id, Order newOrder)
        {
            var status = _ordersController.Update(id, newOrder);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Delete(int id)
        {
            var order = _ordersController.Delete(id);
            if (order.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private bool Put(Order order)
        {
            var status = _ordersController.Put(order);
            if (status.GetType() == typeof(OkObjectResult))
                return true;
            return false;
        }

        private string DescribeTable()
        {
            return string.Format("|{0,5}|{1,25}|{2,14}|", "Id", "Date time", "Menu items");
        }

        private Order GetOne(int id)
        {
            var Orders = _ordersController.GetOne(id);
            if (Orders.GetType() == typeof(OkObjectResult))
            {
                var okResult = Orders as OkObjectResult;
                return okResult.Value as Order;
            }
            return null;
        }

        private IList<Order> GetAll()
        {
            var orders = _ordersController.GetAll();
            var okResult = orders as OkObjectResult;
            var result = okResult.Value as IList<Order>;
            return result.OrderBy(x => x.Id).ToList();
        }
    }
}
