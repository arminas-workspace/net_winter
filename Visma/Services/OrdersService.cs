﻿using System;
using System.Collections.Generic;
using System.Text;
using Visma.Entities;
using Visma.Repositories;

namespace Visma.Services
{
    public class OrdersService : IService<Order>
    {
        private readonly IRepository<Order> _ordersRepository;
        private readonly IRepository<Stock> _stockRepository;
        private readonly IRepository<Menu> _menuRepository;
        public OrdersService()
        {
            _ordersRepository = new OrdersRepository();
            _stockRepository = new StockRepository();
            _menuRepository = new MenuRepository();
        }

        public IList<Order> GetAll()
        {
            return _ordersRepository.GetAll();
        }

        public Order GetOne(int id)
        {
            return _ordersRepository.GetOne(id);
        }

        public bool Put(Order order)
        {
            var check = _ordersRepository.GetOne(order.Id);
            if (check != null)
                return false;
            foreach (var item in order.MenuItems)
            {
                var menuItem = _menuRepository.GetOne(item);
                if (menuItem == null)
                    return false;   //Check if menu item with this id exists
                foreach (var product in menuItem.Products)
                {
                    var stock = _stockRepository.GetOne(product);
                    if (stock == null)
                        return false;   //Check if needed products is in stock
                    //Check if there is enough of needed product and take it away for order
                    if (stock.PortionCount > stock.PortionSize)
                    {
                        stock.PortionCount -= stock.PortionSize;
                        _stockRepository.Update(stock.Id, stock);
                    }
                    else
                        return false;
                }
            }
            return _ordersRepository.Put(order);
        }

        public bool Delete(int id)
        {
            return _ordersRepository.Delete(id);
        }

        public bool Update(int id, Order order)
        {
            var check = _stockRepository.GetOne(order.Id);
            if (check != null && order.Id != id)
                return false;
            foreach (var item in order.MenuItems)
            {
                var menuItem = _menuRepository.GetOne(item);
                if (menuItem == null)
                    return false;   //Check if menu item with this id exists
                foreach (var product in menuItem.Products)
                {
                    var stock = _stockRepository.GetOne(product);
                    if (stock == null)
                        return false;   //Check if needed products is in stock
                    //Check if there is enough of needed product and take it away for order
                    if (stock.PortionCount > stock.PortionSize)
                    {
                        stock.PortionCount -= stock.PortionSize;
                        _stockRepository.Update(stock.Id, stock);
                    }
                    else
                        return false;
                }
            }
            return _ordersRepository.Update(id, order);
        }
    }
}
