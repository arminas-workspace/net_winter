﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Visma.Entities;
using Visma.Repositories;

namespace Visma.Services
{
    public class StockService : IService<Stock>
    {
        private readonly IRepository<Stock> _stockRepository;
        public StockService()
        {
            _stockRepository = new StockRepository();
        }

        public IList<Stock> GetAll()
        {
            return _stockRepository.GetAll();
        }

        public Stock GetOne(int id)
        {
            return _stockRepository.GetOne(id);
        }

        public bool Put(Stock stock)
        {
            var check = _stockRepository.GetOne(stock.Id);
            if (check != null)
                return false;
            return _stockRepository.Put(stock);
        }

        public bool Delete(int id)
        {
            return _stockRepository.Delete(id);
        }

        public bool Update(int id, Stock stock)
        {
            var check = _stockRepository.GetOne(stock.Id);
            if (check != null && stock.Id != id)
                return false;
            return _stockRepository.Update(id, stock);
        }
    }
}
