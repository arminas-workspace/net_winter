﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Visma.Entities;

namespace Visma.Services
{
    public interface IService<T>
    {
        public IList<T> GetAll();
        public T GetOne(int id);
        public bool Put(T item);
        public bool Update(int id, T item);
        public bool Delete(int id);
    }
}
