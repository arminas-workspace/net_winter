﻿using System;
using System.Collections.Generic;
using System.Text;
using Visma.Entities;
using Visma.Repositories;

namespace Visma.Services
{
    public class MenuService : IService<Menu>
    {
        private readonly IRepository<Menu> _menuRepository;
        public MenuService()
        {
            _menuRepository = new MenuRepository();
        }

        public IList<Menu> GetAll()
        {
            return _menuRepository.GetAll();
        }

        public Menu GetOne(int id)
        {
            return _menuRepository.GetOne(id);
        }

        public bool Put(Menu menu)
        {
            var check = _menuRepository.GetOne(menu.Id);
            if (check != null)
                return false;
            return _menuRepository.Put(menu);
        }

        public bool Delete(int id)
        {
            return _menuRepository.Delete(id);
        }

        public bool Update(int id, Menu menu)
        {
            var check = _menuRepository.GetOne(menu.Id);
            if (check != null && menu.Id != id)
                return false;
            return _menuRepository.Update(id, menu);
        }
    }
}
