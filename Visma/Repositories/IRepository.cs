﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Visma.Entities;

namespace Visma.Repositories
{
    public interface IRepository<T>
    {
        public IList<T> GetAll();
        public T GetOne(int id);
        public bool Put(T item);
        public bool Delete(int id);
        public bool Update(int id, T item);
    }
}
