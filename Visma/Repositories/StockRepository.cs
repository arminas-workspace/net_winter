﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Visma.Entities;

namespace Visma.Repositories
{
    public class StockRepository : IRepository<Stock>
    {
        public IList<Stock> GetAll()
        {
            IList<Stock> stock = new List<Stock>();
            using (TextFieldParser parser = new TextFieldParser(@"Stock.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Stock stockTemp = new Stock();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    stockTemp.Id = int.Parse(fields[0]);
                    stockTemp.Name = fields[1];
                    stockTemp.PortionCount = double.Parse(fields[2]);
                    stockTemp.Unit = fields[3];
                    stockTemp.PortionSize = double.Parse(fields[4]);

                    stock.Add(stockTemp);
                }
            }

            return stock;
        }

        public Stock GetOne(int id)
        {
            using (TextFieldParser parser = new TextFieldParser(@"Stock.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    if(int.Parse(fields[0]) == id)
                    {
                        Stock stockTemp = new Stock();
                        stockTemp.Id = int.Parse(fields[0]);
                        stockTemp.Name = fields[1];
                        stockTemp.PortionCount = double.Parse(fields[2]);
                        stockTemp.Unit = fields[3];
                        stockTemp.PortionSize = double.Parse(fields[4]);

                        return stockTemp;
                    }
                }
            }

            return null;
        }

        public bool Put(Stock stock)
        {
            using (StreamWriter w = File.AppendText("Stock.csv"))
            {
                w.WriteLine(string.Format("{0},{1},{2},{3},{4}", stock.Id, stock.Name, stock.PortionCount, stock.Unit, stock.PortionSize));
            }
            return true;
        }

        public bool Delete(int id)
        {
            List<Stock> stock = new List<Stock>();
            using (TextFieldParser parser = new TextFieldParser(@"Stock.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Stock stockTemp = new Stock();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    stockTemp.Id = int.Parse(fields[0]);
                    stockTemp.Name = fields[1];
                    stockTemp.PortionCount = double.Parse(fields[2]);
                    stockTemp.Unit = fields[3];
                    stockTemp.PortionSize = double.Parse(fields[4]);

                    stock.Add(stockTemp);
                }
            }

            var removed = stock.RemoveAll(x => x.Id == id);


            if (removed == 0)
                return false;

            using (StreamWriter w = File.CreateText(@"Stock.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2},{3},{4}", "Id", "Name", "PortionCount", "Unit", "PortionSize"));
                foreach (var item in stock)
                {
                    w.WriteLine(string.Format("{0},{1},{2},{3},{4}", item.Id, item.Name, item.PortionCount, item.Unit, item.PortionSize));
                }
            }

            return true;
        }

        public bool Update(int id, Stock updatedStock)
        {
            List<Stock> stock = new List<Stock>();
            using (TextFieldParser parser = new TextFieldParser(@"Stock.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Stock stockTemp = new Stock();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    stockTemp.Id = int.Parse(fields[0]);
                    stockTemp.Name = fields[1];
                    stockTemp.PortionCount = double.Parse(fields[2]);
                    stockTemp.Unit = fields[3];
                    stockTemp.PortionSize = double.Parse(fields[4]);

                    stock.Add(stockTemp);
                }
            }

            var removed = stock.RemoveAll(x => x.Id == id);

            stock.Add(updatedStock);

            using (StreamWriter w = File.CreateText(@"Stock.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2},{3},{4}", "Id", "Name", "PortionCount", "Unit", "PortionSize"));
                foreach (var item in stock)
                {
                    w.WriteLine(string.Format("{0},{1},{2},{3},{4}", item.Id, item.Name, item.PortionCount, item.Unit, item.PortionSize));
                }
            }

            return true;
        }
    }
}
