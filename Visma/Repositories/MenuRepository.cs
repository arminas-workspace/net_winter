﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Visma.Entities;

namespace Visma.Repositories
{
    public class MenuRepository : IRepository<Menu>
    {
        public IList<Menu> GetAll()
        {
            IList<Menu> menu = new List<Menu>();
            using (TextFieldParser parser = new TextFieldParser(@"Menu.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Menu menuTemp = new Menu();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    menuTemp.Id = int.Parse(fields[0]);
                    menuTemp.Name = fields[1];
                    menuTemp.Products = fields[2].Split(' ').Select(Int32.Parse).ToList();

                    menu.Add(menuTemp);
                }
            }

            return menu;
        }

        public Menu GetOne(int id)
        {
            using (TextFieldParser parser = new TextFieldParser(@"Menu.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (int.Parse(fields[0]) == id)
                    {
                        Menu menuTemp = new Menu();
                        //Processing row
                        menuTemp.Id = int.Parse(fields[0]);
                        menuTemp.Name = fields[1];
                        menuTemp.Products = fields[2].Split(' ').Select(Int32.Parse).ToList();

                        return menuTemp;
                    }
                }
            }

            return null;
        }

        public bool Put(Menu menu)
        {
            using (StreamWriter w = File.AppendText("Menu.csv"))
            {
                w.WriteLine(string.Format("{0},{1},{2}", menu.Id, menu.Name, String.Join(' ', menu.Products.ToArray())));
            }
            return true;
        }

        public bool Delete(int id)
        {
            List<Menu> menu = new List<Menu>();
            using (TextFieldParser parser = new TextFieldParser(@"Menu.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Menu menuTemp = new Menu();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    menuTemp.Id = int.Parse(fields[0]);
                    menuTemp.Name = fields[1];
                    menuTemp.Products = fields[2].Split(' ').Select(Int32.Parse).ToList();

                    menu.Add(menuTemp);
                }
            }

            var removed = menu.RemoveAll(x => x.Id == id);


            if (removed == 0)
                return false;

            using (StreamWriter w = File.CreateText(@"Menu.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2}", "Id", "Name", "Products"));
                foreach (var item in menu)
                {
                    w.WriteLine(string.Format("{0},{1},{2}", item.Id, item.Name, String.Join(' ', item.Products.ToArray())));
                }
            }

            return true;
        }

        public bool Update(int id, Menu updatedMenu)
        {
            List<Menu> menu = new List<Menu>();
            using (TextFieldParser parser = new TextFieldParser(@"Menu.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Menu menuTemp = new Menu();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    menuTemp.Id = int.Parse(fields[0]);
                    menuTemp.Name = fields[1];
                    menuTemp.Products = fields[2].Split(' ').Select(Int32.Parse).ToList();

                    menu.Add(menuTemp);
                }
            }

            var removed = menu.RemoveAll(x => x.Id == id);

            menu.Add(updatedMenu);

            using (StreamWriter w = File.CreateText(@"Menu.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2}", "Id", "Name", "Products"));
                foreach (var item in menu)
                {
                    w.WriteLine(string.Format("{0},{1},{2}", item.Id, item.Name, String.Join(' ', item.Products.ToArray())));
                }
            }

            return true;
        }
    }
}
