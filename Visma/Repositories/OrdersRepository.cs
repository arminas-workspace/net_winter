﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Visma.Entities;

namespace Visma.Repositories
{
    public class OrdersRepository : IRepository<Order>
    {
        public IList<Order> GetAll()
        {
            IList<Order> orders = new List<Order>();
            using (TextFieldParser parser = new TextFieldParser(@"Orders.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Order orderTemp = new Order();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    orderTemp.Id = int.Parse(fields[0]);
                    orderTemp.DateTime = DateTime.Parse(fields[1]);
                    orderTemp.MenuItems = fields[2].Split(' ').Select(Int32.Parse).ToList();


                    orders.Add(orderTemp);
                }
            }

            return orders;
        }

        public Order GetOne(int id)
        {
            using (TextFieldParser parser = new TextFieldParser(@"Orders.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (int.Parse(fields[0]) == id)
                    {
                        Order orderTemp = new Order();
                        //Processing row
                        orderTemp.Id = int.Parse(fields[0]);
                        orderTemp.DateTime = DateTime.Parse(fields[1]);
                        orderTemp.MenuItems = fields[2].Split(' ').Select(Int32.Parse).ToList();

                        return orderTemp;
                    }
                }
            }

            return null;
        }

        public bool Put(Order order)
        {
            using (StreamWriter w = File.AppendText("Orders.csv"))
            {
                w.WriteLine(string.Format("{0},{1},{2}", order.Id, order.DateTime, String.Join(' ', order.MenuItems.ToArray())));
            }
            return true;
        }

        public bool Delete(int id)
        {
            List<Order> orders = new List<Order>();
            using (TextFieldParser parser = new TextFieldParser(@"Orders.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Order orderTemp = new Order();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    orderTemp.Id = int.Parse(fields[0]);
                    orderTemp.DateTime = DateTime.Parse(fields[1]);
                    orderTemp.MenuItems = fields[2].Split(' ').Select(Int32.Parse).ToList();

                    orders.Add(orderTemp);
                }
            }

            var removed = orders.RemoveAll(x => x.Id == id);


            if (removed == 0)
                return false;

            using (StreamWriter w = File.CreateText(@"Orders.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2}", "Id", "Date time", "Menu items"));
                foreach (var item in orders)
                {
                    w.WriteLine(string.Format("{0},{1},{2}", item.Id, item.DateTime, String.Join(' ', item.MenuItems.ToArray())));
                }
            }

            return true;
        }

        public bool Update(int id, Order updatedOrder)
        {
            List<Order> orders = new List<Order>();
            using (TextFieldParser parser = new TextFieldParser(@"Orders.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadFields();    //Skip first line
                while (!parser.EndOfData)
                {
                    Order orderTemp = new Order();
                    //Processing row
                    string[] fields = parser.ReadFields();
                    orderTemp.Id = int.Parse(fields[0]);
                    orderTemp.DateTime = DateTime.Parse(fields[1]);
                    orderTemp.MenuItems = fields[2].Split(' ').Select(Int32.Parse).ToList();

                    orders.Add(orderTemp);
                }
            }

            var removed = orders.RemoveAll(x => x.Id == id);

            orders.Add(updatedOrder);

            using (StreamWriter w = File.CreateText(@"Orders.csv"))
            {
                //first line
                w.WriteLine(string.Format("{0},{1},{2}", "Id", "Date time", "Menu items"));
                foreach (var item in orders)
                {
                    w.WriteLine(string.Format("{0},{1},{2}", item.Id, item.DateTime, String.Join(' ', item.MenuItems.ToArray())));
                }
            }

            return true;
        }
    }
}
