﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using Visma.Controllers;
using Visma.Entities;
using Visma.Watchers;

namespace Visma
{
    class Program
    {
        static void Main(string[] args)
        {
            IWatcher stockWatcher = new StockWatcher();
            IWatcher menuWatcher = new MenuWatcher();
            IWatcher ordersWatcher = new OrdersWatcher();
            Console.WriteLine(">You are now in base menu");

            string[] commands = new string[]{ "stock", "menu", "orders", "exit" };
            string input = "";

            while(input.ToLower().CompareTo("exit") != 0)
            {
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "stock":
                        Console.WriteLine(">Set to use stock table");
                        stockWatcher.BeginWatching();
                        break;
                    case "menu":
                        Console.WriteLine(">Set to use menu table");
                        menuWatcher.BeginWatching();
                        break;
                    case "orders":
                        Console.WriteLine(">Set to use orders table");
                        ordersWatcher.BeginWatching();
                        break;
                    case "exit":
                        Console.WriteLine(">Exiting the program");
                        break;
                    default:
                        Console.WriteLine(">Invalid command, please input using the correct format!");
                        break;
                }
            }
        }
    }
}
